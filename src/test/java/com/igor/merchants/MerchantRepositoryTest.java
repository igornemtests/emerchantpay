package com.igor.merchants;

import java.math.BigDecimal;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.junit.Assert;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.Rollback;

import com.igor.merchants.dal.entity.Merchant;
import com.igor.merchants.dal.entity.MerchantStatus;
import com.igor.merchants.dal.repository.MerchantRepository;
import com.igor.merchants.service.MerchantsLoader;
import com.igor.merchants.service.UsersLoader;

@DataJpaTest
@Transactional
@Rollback(false)
@TestMethodOrder(OrderAnnotation.class)
public class MerchantRepositoryTest {

    private static final String TEST = "test";
    @Autowired
    private MerchantRepository repo;
    @MockBean
    private MerchantsLoader merchantsLoader;
    @MockBean
    private UsersLoader usersLoader;
    @MockBean
    private MerchantsApplication merchantsApplication;
    @PersistenceContext
    private EntityManager entityManager;

    @Test
    @Order(1)
    public void testCreateMerchant() {
	Merchant entity = new Merchant(TEST, TEST, TEST + "@" + TEST + "." + TEST, MerchantStatus.ACTIVE,
		BigDecimal.ZERO);
	repo.save(entity);
	entityManager.flush();
	entityManager.detach(entity);
	Merchant found = repo.findAll().iterator().next();
	Assert.assertEquals(found.getName(), entity.getName());
    }

    @Test
    @Order(2)
    public void testUpdateMerchant() {
	Merchant entity = repo.findAll().iterator().next();
	String newDescription = TEST + "updated";
	entity.setDescription(newDescription);
	repo.save(entity);
	entityManager.flush();
	entityManager.detach(entity);
	Merchant found = repo.findAll().iterator().next();
	Assert.assertEquals(found.getName(), entity.getName());
    }

    @Test
    @Order(3)
    public void testDeleteMerchant() {
	Merchant entity = repo.findAll().iterator().next();
	String newDescription = TEST + "updated";
	entity.setDescription(newDescription);
	repo.delete(entity);
	entityManager.flush();
	Assert.assertFalse(repo.findAll().iterator().hasNext());
    }
}
