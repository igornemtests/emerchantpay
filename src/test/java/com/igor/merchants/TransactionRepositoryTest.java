package com.igor.merchants;

import java.math.BigDecimal;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.junit.Assert;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.Rollback;

import com.igor.merchants.dal.entity.transactions.AuthorizeTransaction;
import com.igor.merchants.dal.entity.transactions.ChargeTransaction;
import com.igor.merchants.dal.entity.transactions.ErrorTransaction;
import com.igor.merchants.dal.entity.transactions.RefundTransaction;
import com.igor.merchants.dal.entity.transactions.ReversalTransaction;
import com.igor.merchants.dal.entity.transactions.Transaction;
import com.igor.merchants.dal.repository.TransactionRepository;
import com.igor.merchants.service.MerchantsLoader;
import com.igor.merchants.service.UsersLoader;

@DataJpaTest
@Transactional
@Rollback(false)
@TestMethodOrder(OrderAnnotation.class)
public class TransactionRepositoryTest {

    private static final String AUTH_UUID = "00000000-0000-0000-0000-000000000001";
    private static final String TEST = "test";
    @Autowired
    private TransactionRepository repo;
    @MockBean
    private MerchantsLoader merchantsLoader;
    @MockBean
    private UsersLoader usersLoader;
    @MockBean
    private MerchantsApplication merchantsApplication;
    @PersistenceContext
    private EntityManager entityManager;

    @Test
    @Order(1)
    public void testCreateAuthorizeTransaction() {
	UUID uuid = UUID.fromString(AUTH_UUID);
	AuthorizeTransaction at = new AuthorizeTransaction(uuid, BigDecimal.ZERO, TEST, TEST);
	repo.save(at);
	entityManager.flush();
	entityManager.detach(at);
	Transaction found = repo.findByUuid(uuid);
	Assert.assertEquals(found.getCustomerEmail(), at.getCustomerEmail());
    }

    @Test
    @Order(2)
    public void testCreateChargeTransaction() {
	UUID authUuid = UUID.fromString(AUTH_UUID);
	UUID chargeUuid = UUID.fromString(AUTH_UUID.replace("1", "2"));
	AuthorizeTransaction at = (AuthorizeTransaction) repo.findByUuid(authUuid);
	ChargeTransaction ct = new ChargeTransaction(chargeUuid, authUuid, new BigDecimal(10), TEST, TEST);
	ct.setOrigin(at);
	repo.save(ct);
	entityManager.flush();
	entityManager.detach(ct);
	entityManager.detach(at);
	ChargeTransaction found = (ChargeTransaction) repo.findByUuid(chargeUuid);
	Assert.assertEquals(found.getCustomerEmail(), at.getCustomerEmail());
	Assert.assertEquals(found.getOrigin().getUuid(), authUuid);
    }

    @Test
    @Order(3)
    public void testCreateRefundTransaction() {
	UUID chargeUuid = UUID.fromString(AUTH_UUID.replace("1", "2"));
	UUID refundUuid = UUID.fromString(AUTH_UUID.replace("1", "3"));
	ChargeTransaction ct = (ChargeTransaction) repo.findByUuid(chargeUuid);
	RefundTransaction rt = new RefundTransaction(refundUuid, refundUuid, TEST, TEST);
	rt.setOrigin(ct);
	repo.save(rt);
	entityManager.flush();
	entityManager.detach(ct);
	entityManager.detach(rt);
	RefundTransaction found = (RefundTransaction) repo.findByUuid(refundUuid);
	Assert.assertEquals(found.getCustomerEmail(), rt.getCustomerEmail());
	Assert.assertEquals(found.getOrigin().getUuid(), chargeUuid);
    }

    @Test
    @Order(4)
    public void testCreateReversalTransaction() {
	UUID authUuid = UUID.fromString(AUTH_UUID);
	UUID reversalUuid = UUID.fromString(AUTH_UUID.replace("1", "4"));
	AuthorizeTransaction at = (AuthorizeTransaction) repo.findByUuid(authUuid);
	ReversalTransaction rt = new ReversalTransaction(reversalUuid, authUuid, TEST, TEST);
	rt.setOrigin(at);
	repo.save(rt);
	entityManager.flush();
	entityManager.detach(at);
	entityManager.detach(rt);
	ReversalTransaction found = (ReversalTransaction) repo.findByUuid(reversalUuid);
	Assert.assertEquals(found.getCustomerEmail(), rt.getCustomerEmail());
	Assert.assertEquals(found.getOrigin().getUuid(), authUuid);
    }

    @Test
    @Order(5)
    public void testCreateErrorTransaction() {
	UUID authUuid = UUID.fromString(AUTH_UUID);
	UUID errorUuid = UUID.fromString(AUTH_UUID.replace("1", "5"));
	AuthorizeTransaction at = (AuthorizeTransaction) repo.findByUuid(authUuid);
	ErrorTransaction et = new ErrorTransaction(errorUuid, at.getAmount(), TEST, TEST);
	et.setOrigin(at);
	repo.save(et);
	entityManager.flush();
	entityManager.detach(at);
	entityManager.detach(et);
	ErrorTransaction found = (ErrorTransaction) repo.findByUuid(errorUuid);
	Assert.assertEquals(found.getCustomerEmail(), et.getCustomerEmail());
	Assert.assertEquals(found.getOrigin().getUuid(), authUuid);
    }

}
