package com.igor.merchants;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.igor.merchants.dal.entity.Merchant;
import com.igor.merchants.dal.entity.MerchantStatus;
import com.igor.merchants.dal.entity.transactions.AuthorizeTransaction;
import com.igor.merchants.dal.entity.transactions.ChargeTransaction;
import com.igor.merchants.dal.entity.transactions.RefundTransaction;
import com.igor.merchants.dal.entity.transactions.ReversalTransaction;
import com.igor.merchants.dal.entity.transactions.Transaction;
import com.igor.merchants.dal.entity.transactions.TransactionStatus;
import com.igor.merchants.dal.repository.TransactionRepository;
import com.igor.merchants.service.MerchantSevice;
import com.igor.merchants.service.MerchantsLoader;
import com.igor.merchants.service.TransactionSevice;
import com.igor.merchants.service.UsersLoader;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
public class TransactionServiceTest {

    private static final String AUTH_UUID = "00000000-0000-0000-0000-000000000001";
    private static final String TEST = "test";
    @Autowired
    private TransactionSevice trns;
    @Autowired
    private MerchantSevice merchants;
    @Autowired
    private TransactionRepository trnRepo;

    @MockBean
    private MerchantsLoader merchantsLoader;
    @MockBean
    private UsersLoader usersLoader;
    @MockBean
    private MerchantsApplication merchantsApplication;
    private static RefundTransaction refundTransaction;
    private static ChargeTransaction chargeTransaction;
    private static AuthorizeTransaction authTransaction;
    private static ReversalTransaction reversalErrorTransaction;

    private static Merchant merchantActive;
    private static Merchant merchantInactive;

    @BeforeEach
    public void init() {
	if (merchantActive == null) {
	    merchantActive = merchants.addMerchant(
		    new Merchant(TEST, TEST, TEST + "@" + TEST + "." + TEST, MerchantStatus.ACTIVE, BigDecimal.ZERO));
	    merchantInactive = merchants.addMerchant(
		    new Merchant(TEST, TEST, TEST + "@" + TEST + "." + TEST, MerchantStatus.INACTIVE, BigDecimal.ZERO));
	}
    }

    @Test
    @Order(1)
    public void givenInactiveMerchantAndAuthorizeTransactionWhenCreateTransactionThenFail() {
	UUID uuid = UUID.fromString(AUTH_UUID);
	AuthorizeTransaction t = new AuthorizeTransaction(uuid, BigDecimal.ONE, TEST, TEST);
	RuntimeException exception = assertThrows(RuntimeException.class, () -> {
	    trns.createTransaction(merchantInactive.getId(), t);
	});
	String expectedMessage = "Merchant " + merchantInactive.getName() + " inactive";
	String actualMessage = exception.getMessage();
	assertEquals(actualMessage, expectedMessage);
    }

    @Test
    @Order(2)
    public void givenNotExistingMerchantAndAuthorizeTransactionWhenCreateTransactionThenFail() {
	long merchantId = -1L;
	UUID uuid = UUID.fromString(AUTH_UUID);
	AuthorizeTransaction t = new AuthorizeTransaction(uuid, BigDecimal.ONE, TEST, TEST);
	RuntimeException exception = assertThrows(RuntimeException.class, () -> {
	    trns.createTransaction(merchantId, t);
	});
	String expectedMessage = "Merchant with id: " + merchantId + " not found";
	String actualMessage = exception.getMessage();
	assertEquals(actualMessage, expectedMessage);
    }

    @Test
    @Order(3)
    public void givenNotCorrectAuthorizeTransactionWhenCreateTransactionThenFail() {
	UUID uuid = UUID.fromString(AUTH_UUID);
	AuthorizeTransaction t = new AuthorizeTransaction(uuid, BigDecimal.ZERO, TEST, TEST);
	RuntimeException exception = assertThrows(RuntimeException.class, () -> {
	    trns.createTransaction(merchantActive.getId(), t);
	});
	String expectedMessage = "Amount should be > 0";
	String actualMessage = exception.getMessage();
	assertEquals(actualMessage, expectedMessage);
    }

    /**
     * Compare two lists they should contain same transactions
     */
    private void assertSame(Transaction... expected) {
	Iterable<Transaction> actual = trnRepo.findAll();
	assertNotNull(actual);
	assertTrue(actual.iterator().hasNext());
	List<Transaction> actuals = StreamSupport.stream(actual.spliterator(), false).collect(Collectors.toList());
	assertEquals(actuals.size(), expected.length);
	for (Transaction eT : expected) {
	    assertTrue(actuals.contains(new Transaction() {
		public boolean equals(Object trn) {
		    return (((Transaction) trn).getUuid().equals(eT.getUuid()));
		}
	    }));
	}
    }

    @Test
    @Order(4)
    public void givenAuthorizeTransactionWhenCreateTransactionThenSuccess() {
	UUID uuid = UUID.fromString(AUTH_UUID);
	authTransaction = new AuthorizeTransaction(uuid, BigDecimal.ONE, TEST, TEST);
	trns.createTransaction(merchantActive.getId(), authTransaction);
	assertSame(authTransaction);
    }

    @Test
    @Order(5)
    public void givenChargeTransactionWhenCreateTransactionThenSuccess() {
	UUID uuid = UUID.fromString(AUTH_UUID.replace("1", "2"));
	chargeTransaction = new ChargeTransaction(uuid, authTransaction.getUuid(), BigDecimal.ONE, TEST, TEST);
	trns.createTransaction(merchantActive.getId(), chargeTransaction);
	assertSame(authTransaction, chargeTransaction);
    }

    @Test
    @Order(6)
    public void givenReversalTransactionWhenCreateTransactionThenErrorTransactionCreated() {
	UUID uuid = UUID.fromString(AUTH_UUID.replace("1", "3"));
	reversalErrorTransaction = new ReversalTransaction(uuid, authTransaction.getUuid(), TEST, TEST);
	trns.createTransaction(merchantActive.getId(), reversalErrorTransaction);
	reversalErrorTransaction.setStatus(TransactionStatus.ERROR);
	assertSame(authTransaction, chargeTransaction, reversalErrorTransaction);
    }

    @Test
    @Order(7)
    public void givenRefundTransactionWhenCreateTransactionThenSuccess() {
	UUID uuid = UUID.fromString(AUTH_UUID.replace("1", "4"));
	refundTransaction = new RefundTransaction(uuid, chargeTransaction.getUuid(), TEST, TEST);
	trns.createTransaction(merchantActive.getId(), refundTransaction);
	assertSame(authTransaction, chargeTransaction, reversalErrorTransaction, refundTransaction);
    }

    @Test
    @Order(8)
    public void givenReverseTransactionWhenCreateTransactionThenSuccess() {
	UUID uuid = UUID.fromString(AUTH_UUID.replace("1", "5"));
	ReversalTransaction reversalTransaction = new ReversalTransaction(uuid, authTransaction.getUuid(), TEST, TEST);
	trns.createTransaction(merchantActive.getId(), reversalTransaction);
	assertSame(authTransaction, chargeTransaction, reversalErrorTransaction, refundTransaction,
		reversalTransaction);
    }
}