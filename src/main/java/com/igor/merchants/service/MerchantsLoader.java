package com.igor.merchants.service;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.igor.merchants.dal.entity.Merchant;
import com.igor.merchants.dal.repository.MerchantRepository;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;

/**
 * Loads merchants from CSV file at the application boot.
 *
 */
@Component
public class MerchantsLoader {

  @Autowired
  private MerchantRepository repository;

  /**
   * Import merchants from CSV file into DB.
   * 
   * @param filePath - CSV file path to import from
   * @throws FileNotFoundException
   * @throws IOException
   */
  public void load(String filePath) throws FileNotFoundException, IOException {
    try (Reader reader = new BufferedReader(new FileReader(filePath))) {

      CsvToBean<Merchant> csvToBean = new CsvToBeanBuilder<Merchant>(reader).withType(Merchant.class).withIgnoreLeadingWhiteSpace(true)
          .build();

      for (Merchant merchant : csvToBean) {
        repository.save(merchant);
      }
    }
  }
}
