package com.igor.merchants.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.igor.merchants.dal.entity.Merchant;
import com.igor.merchants.dal.repository.MerchantRepository;
import com.igor.merchants.dal.repository.TransactionRepository;

@Service
public class MerchantSevice {
    @Autowired
    private MerchantRepository repo;

    @Autowired
    private TransactionRepository trnRepo;

    public Iterable<Merchant> getMerchants() {
	return repo.findAllByOrderByName();
    }

    public Merchant addMerchant(Merchant merchant) {
	return repo.save(merchant);
    }

    public Merchant updateMerchant(Merchant merchant) {
	Merchant foundMerchant = repo.findById(merchant.getId())
		.orElseThrow(() -> new RuntimeException("Merchant with Id:" + merchant.getId() + " not found"));
	foundMerchant.setName(merchant.getName());
	foundMerchant.setDescription(merchant.getDescription());
	foundMerchant.setEmail(merchant.getEmail());
	foundMerchant.setStatus(merchant.getStatus());
	foundMerchant.setStatus(merchant.getStatus());
	return repo.save(foundMerchant);
    }

    public void removeMerchant(Long id) {
	Long trnCount = trnRepo.countByMerchantId(id);
	if (trnCount != null && trnCount > 0) {
	    Merchant merchant = repo.findById(id)
		    .orElseThrow(() -> new RuntimeException("Merchant with ID:" + id + " doesn't exists"));
	    throw new RuntimeException("Merchant " + merchant.getName() + " has transactions and can't be removed");
	}
	repo.deleteById(id);
    }

    public Merchant getMerchant(Long merchantId) {
	return repo.findById(merchantId)
		.orElseThrow(() -> new RuntimeException("Merchant " + merchantId + " not found"));
    }

}
