package com.igor.merchants.service;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Calendar;
import java.util.UUID;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import com.igor.merchants.dal.entity.Merchant;
import com.igor.merchants.dal.entity.MerchantStatus;
import com.igor.merchants.dal.entity.Role;
import com.igor.merchants.dal.entity.User;
import com.igor.merchants.dal.entity.transactions.AuthorizeTransaction;
import com.igor.merchants.dal.entity.transactions.BelongingTransaction;
import com.igor.merchants.dal.entity.transactions.ChargeTransaction;
import com.igor.merchants.dal.entity.transactions.RefundTransaction;
import com.igor.merchants.dal.entity.transactions.ReversalTransaction;
import com.igor.merchants.dal.entity.transactions.Transaction;
import com.igor.merchants.dal.entity.transactions.TransactionStatus;
import com.igor.merchants.dal.repository.MerchantRepository;
import com.igor.merchants.dal.repository.TransactionRepository;
import com.igor.merchants.exception.TransactionCreationException;

@Service
@Transactional
@Validated
public class TransactionSevice {
    @Autowired
    private TransactionRepository transactionRepo;

    @Autowired
    private MerchantRepository merchantRepo;

    @Value("${transaction.expiration}")
    private int expired;

    public Iterable<Transaction> getTransactions(User user) {
	if (user.getRole() == Role.ADMIN) {
	    return transactionRepo.findAllByOrderByMerchantIdAscId();
	} else {
	    return transactionRepo.findAllByMerchantIdOrderById(user.getMerchantId());
	}
    }

    private void create(ChargeTransaction trn, AuthorizeTransaction authorizeTrn) throws TransactionCreationException {
	BigDecimal charged = transactionRepo.getChargedSummary(authorizeTrn);
	if (charged != null
		&& authorizeTrn.getAmount().subtract(charged.add(trn.getAmount())).compareTo(BigDecimal.ZERO) < 0) {
	    throw new TransactionCreationException("Not enough authorized amount");
	}
	Merchant merchant = authorizeTrn.getMerchant();
	merchant.setTotalTransactionSum(merchant.getTotalTransactionSum().add(trn.getAmount()));
	merchantRepo.save(merchant);
    }

    private void create(RefundTransaction trn, ChargeTransaction chargeTrn) {
	trn.setAmount(chargeTrn.getAmount());
	chargeTrn.setStatus(TransactionStatus.REFUNDED);
	transactionRepo.save(chargeTrn);
	Merchant merchant = chargeTrn.getMerchant();
	merchant.setTotalTransactionSum(merchant.getTotalTransactionSum().subtract(chargeTrn.getAmount()));
	merchantRepo.save(merchant);
    }

    private void create(ReversalTransaction trn, AuthorizeTransaction authorizeTrn)
	    throws TransactionCreationException {
	BigDecimal charged = transactionRepo.getChargedSummary(authorizeTrn);
	if (charged != null && charged.compareTo(BigDecimal.ZERO) != 0) {
	    throw new TransactionCreationException(
		    "Cannot reverse, there are not refunded charges, please refund first");
	}
	authorizeTrn.setStatus(TransactionStatus.REVERSED);
	trn.setAmount(authorizeTrn.getAmount());
	transactionRepo.save(authorizeTrn);
    }

    public Transaction createTransaction(Long merchantId, @Valid Transaction trn) {
	if ((trn instanceof AuthorizeTransaction || trn instanceof ChargeTransaction)
		&& (trn.getAmount() == null || BigDecimal.ZERO.compareTo(trn.getAmount()) >= 0)) {
	    throw new RuntimeException("Amount should be > 0");
	}

	Merchant merchant = merchantRepo.findById(merchantId)
		.orElseThrow(() -> new RuntimeException("Merchant with id: " + merchantId + " not found"));
	if (merchant.getStatus() != MerchantStatus.ACTIVE) {
	    throw new RuntimeException("Merchant " + merchant.getName() + " inactive");
	}
	trn.setMerchant(merchant);
	if (trn instanceof BelongingTransaction) {
	    BelongingTransaction<Transaction> t = (BelongingTransaction<Transaction>) trn;
	    UUID originUUID = t.getOriginUuid();
	    Transaction originTrn = transactionRepo.findByUuid(originUUID);
	    t.setOrigin(originTrn);
	    if (originTrn == null) {
		throw new RuntimeException("Origin transaction " + originUUID + " not found");
	    }
	    try {
		if (ChargeTransaction.class == trn.getClass()) {
		    create((ChargeTransaction) trn, (AuthorizeTransaction) originTrn);
		} else if (RefundTransaction.class == trn.getClass()) {
		    create((RefundTransaction) trn, (ChargeTransaction) originTrn);
		} else if (ReversalTransaction.class == trn.getClass()) {
		    create((ReversalTransaction) trn, (AuthorizeTransaction) originTrn);
		}
	    } catch (TransactionCreationException te) {
		trn.setStatus(TransactionStatus.ERROR);
	    }
	    originTrn.setChild(t);
	}
	return transactionRepo.save(trn);
    }

    public void deleteExpired() {
	Calendar now = Calendar.getInstance();
	now.add(Calendar.MINUTE, -expired);
	transactionRepo.deleteByCreatedBefore(new Date(now.getTimeInMillis()));
    }

}
