package com.igor.merchants.dal.repository;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.UUID;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.igor.merchants.dal.entity.transactions.Transaction;

public interface TransactionRepository extends CrudRepository<Transaction, Long> {

    Transaction findByUuid(UUID originTrn);

    Long countByMerchantId(Long id);

    @Query("SELECT SUM(t.amount) FROM ChargeTransaction t where t.origin = :origin and t.status = 0")
    BigDecimal getChargedSummary(Transaction origin);

    Iterable<Transaction> findAllByOrderByMerchantIdAscId();

    Iterable<Transaction> findAllByMerchantIdOrderById(Long merchantId);

    @Modifying
    void deleteByCreatedBefore(Date expiryDate);

}
