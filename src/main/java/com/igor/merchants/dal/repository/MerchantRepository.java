package com.igor.merchants.dal.repository;

import org.springframework.data.repository.CrudRepository;

import com.igor.merchants.dal.entity.Merchant;

public interface MerchantRepository extends CrudRepository<Merchant, Long> {

    Iterable<Merchant> findAllByOrderByName();

}
