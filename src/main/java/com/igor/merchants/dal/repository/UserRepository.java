package com.igor.merchants.dal.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.igor.merchants.dal.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {
  User findByUsername(String username);
}
