package com.igor.merchants.dal.entity.transactions;

import java.math.BigDecimal;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PreRemove;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.igor.merchants.dal.entity.Merchant;
import com.igor.merchants.dal.entity.base.BaseEntity;

/**
 * Transaction entity
 */
@Table(name = "transactions")
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@SequenceGenerator(name = BaseEntity.ID_GEN, sequenceName = "trn_seq", allocationSize = 1)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "type", defaultImpl = Transaction.class)
@JsonSubTypes({ @JsonSubTypes.Type(value = AuthorizeTransaction.class, name = Transaction.Discriminator.AUTHORIZE),
	@JsonSubTypes.Type(value = ChargeTransaction.class, name = Transaction.Discriminator.CHARGE),
	@JsonSubTypes.Type(value = RefundTransaction.class, name = Transaction.Discriminator.REFUND),
	@JsonSubTypes.Type(value = ReversalTransaction.class, name = Transaction.Discriminator.REVERSAL), })
public class Transaction extends BaseEntity {

    public interface Discriminator {
	String AUTHORIZE = "A";
	String CHARGE = "C";
	String REFUND = "B";
	String REVERSAL = "R";
	String ERROR = "E";
    }

    public static final String MERCHANT_ID_FIELD_NAME = "merchant_id";

    @Column(unique = true, nullable = false)
    private UUID uuid;

    @JsonIgnore
    @OneToOne(targetEntity = BelongingTransaction.class)
    private BelongingTransaction<?> child;

    @PreRemove
    private void preRemove() {
	if (child != null) {
	    child.setOrigin(null);
	}
    }

    private BigDecimal amount;

    private TransactionStatus status = TransactionStatus.APPROVED;

    @Email(message = "CustomerEmail must have correct format")
    @NotBlank(message = "CustomerEmail is mandatory")
    @Column(nullable = false)
    private String customerEmail;

    private String customerPhone;

    @Column(name = "type", insertable = false, updatable = false)
    private String type;

    @ManyToOne
    @JoinColumn(name = MERCHANT_ID_FIELD_NAME)
    private Merchant merchant;

    public Transaction() {

    }

    public Transaction(UUID uuid, BigDecimal amount, String customerEmail, String customerPhone) {
	this();
	this.uuid = uuid;
	this.amount = amount;
	this.customerEmail = customerEmail;
	this.customerPhone = customerPhone;
    }

    public UUID getUuid() {
	return uuid;
    }

    public void setUuid(UUID uuid) {
	this.uuid = uuid;
    }

    public TransactionStatus getStatus() {
	return status;
    }

    public void setStatus(TransactionStatus status) {
	this.status = status;
    }

    public String getCustomerEmail() {
	return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
	this.customerEmail = customerEmail;
    }

    public String getCustomerPhone() {
	return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
	this.customerPhone = customerPhone;
    }

    public BigDecimal getAmount() {
	return amount;
    }

    public void setAmount(BigDecimal amount) {
	this.amount = amount;
    }

    public String getType() {
	return type;
    }

    public void setType(String type) {
	this.type = type;
    }

    public Merchant getMerchant() {
	return merchant;
    }

    public void setMerchant(Merchant merchant) {
	this.merchant = merchant;
    }

    public BelongingTransaction<?> getChild() {
	return child;
    }

    public void setChild(BelongingTransaction<?> child) {
	this.child = child;
    }

}