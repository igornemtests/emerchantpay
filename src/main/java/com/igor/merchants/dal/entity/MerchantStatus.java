package com.igor.merchants.dal.entity;

public enum MerchantStatus {
	ACTIVE, INACTIVE;
}