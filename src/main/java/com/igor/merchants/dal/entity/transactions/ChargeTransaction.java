package com.igor.merchants.dal.entity.transactions;

import java.math.BigDecimal;
import java.util.UUID;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@DiscriminatorValue(Transaction.Discriminator.CHARGE)
public class ChargeTransaction extends BelongingTransaction<AuthorizeTransaction> {
    public ChargeTransaction() {
    }

    @JsonCreator
    public ChargeTransaction(@JsonProperty(required = true) UUID uuid, @JsonProperty(required = true) UUID originUuid,
	    @JsonProperty(required = true) BigDecimal amount, @JsonProperty(required = true) String customerEmail,
	    String customerPhone) {
	super(uuid, originUuid, amount, customerEmail, customerPhone);
    }

}
