package com.igor.merchants.dal.entity.transactions;

import java.math.BigDecimal;
import java.util.UUID;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(Transaction.Discriminator.ERROR)
public class ErrorTransaction extends BelongingTransaction<Transaction> {
    public ErrorTransaction() {
    }

    public ErrorTransaction(UUID uuid, BigDecimal amount, String customerEmail, String customerPhone) {
	super(uuid, null, amount, customerEmail, customerPhone);
	setStatus(TransactionStatus.ERROR);
    }
}
