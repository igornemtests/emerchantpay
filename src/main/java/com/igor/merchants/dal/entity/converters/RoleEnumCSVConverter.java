package com.igor.merchants.dal.entity.converters;

import com.igor.merchants.dal.entity.Role;
import com.opencsv.bean.AbstractBeanField;
import com.opencsv.exceptions.CsvConstraintViolationException;
import com.opencsv.exceptions.CsvDataTypeMismatchException;

public class RoleEnumCSVConverter extends AbstractBeanField<Role, String> {
    @Override
    protected Role convert(String value) throws CsvDataTypeMismatchException, CsvConstraintViolationException {
	try {
	    return Role.valueOf(value.toUpperCase());
	} catch (RuntimeException e) {
	    throw new CsvDataTypeMismatchException(e.getMessage());
	}
    }
}
