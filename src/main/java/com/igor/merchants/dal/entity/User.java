package com.igor.merchants.dal.entity;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.igor.merchants.dal.entity.base.BaseEntity;
import com.igor.merchants.dal.entity.converters.RoleEnumCSVConverter;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvCustomBindByName;

@Table(name = "users")
@Entity
@SequenceGenerator(name = BaseEntity.ID_GEN, sequenceName = "user_seq", allocationSize = 1)
public class User extends BaseEntity implements UserDetails {
    @CsvBindByName
    private String username;
    @CsvBindByName
    private String password;
    @CsvBindByName
    private Long merchantId;

    @CsvCustomBindByName(converter = RoleEnumCSVConverter.class)
    @Enumerated(EnumType.STRING)
    private Role role;

    public boolean isAdmin() {
	return role == Role.ADMIN;
    }

    public String getUsername() {
	return username;
    }

    @Override
    public boolean isAccountNonExpired() {
	return true;
    }

    @Override
    public boolean isAccountNonLocked() {
	return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
	return true;
    }

    @Override
    public boolean isEnabled() {
	return true;
    }

    public void setUsername(String username) {
	this.username = username;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
	return new HashSet(Arrays.asList(getRole()));
    }

    public String getPassword() {
	return password;
    }

    public void setPassword(final String password) {
	this.password = password;
    }

    public Role getRole() {
	return role;
    }

    public void setRole(Role role) {
	this.role = role;
    }

    public Long getMerchantId() {
	return merchantId;
    }

    public void setMerchantId(Long merchantId) {
	this.merchantId = merchantId;
    }
}
