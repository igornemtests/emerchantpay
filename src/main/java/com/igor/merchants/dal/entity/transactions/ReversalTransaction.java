package com.igor.merchants.dal.entity.transactions;

import java.util.UUID;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@DiscriminatorValue(Transaction.Discriminator.REVERSAL)
public class ReversalTransaction extends BelongingTransaction<AuthorizeTransaction> {
    public ReversalTransaction() {
    }

    @JsonCreator
    public ReversalTransaction(@JsonProperty(required = true) UUID uuid, @JsonProperty(required = true) UUID originUuid,
	    @JsonProperty(required = true) String customerEmail, String customerPhone) {
	super(uuid, originUuid, null, customerEmail, customerPhone);
    }
}
