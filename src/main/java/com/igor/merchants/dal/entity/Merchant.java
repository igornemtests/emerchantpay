package com.igor.merchants.dal.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Formula;

import com.igor.merchants.dal.entity.base.BaseEntity;
import com.igor.merchants.dal.entity.transactions.Transaction;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvCustomBindByName;

@Entity
@SequenceGenerator(name = BaseEntity.ID_GEN, sequenceName = "merchant_seq", allocationSize = 1)
public class Merchant extends BaseEntity {
    @CsvBindByName
    @NotBlank(message = "Name is mandatory")
    private String name;

    @CsvBindByName
    private String description;

    @CsvBindByName
    @Email(message = "Email must have correct format")
    @NotBlank(message = "Email is mandatory")
    private String email;

    @Enumerated(EnumType.ORDINAL)
    @CsvCustomBindByName(converter = MerchantStatusCSVConverter.class)
    @NotNull(message = "Status is mandatory")
    private MerchantStatus status;

    @CsvBindByName
    private BigDecimal totalTransactionSum = BigDecimal.ZERO;

    @Formula("(select count(*) from Transactions t where t." + Transaction.MERCHANT_ID_FIELD_NAME + " = id)")
    private Integer transactionsCount;

    public Merchant() {
    }

    public Merchant(String name, String description, String email, MerchantStatus status,
	    BigDecimal totalTransactionSum) {
	this.name = name;
	this.description = description;
	this.email = email;
	this.status = status;
	this.totalTransactionSum = totalTransactionSum;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public MerchantStatus getStatus() {
	return status;
    }

    public void setStatus(MerchantStatus status) {
	this.status = status;
    }

    public BigDecimal getTotalTransactionSum() {
	return totalTransactionSum;
    }

    public void setTotalTransactionSum(BigDecimal totalTransactionSum) {
	this.totalTransactionSum = totalTransactionSum;
    }

    public Integer getTransactionsCount() {
	return transactionsCount;
    }

    public void setTransactionsCount(Integer transactionsCount) {
	this.transactionsCount = transactionsCount;
    }

}
