package com.igor.merchants.dal.entity.base;

import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;

import org.springframework.data.annotation.CreatedDate;

/**
 * Super class for all entities.
 *
 */
@MappedSuperclass
public abstract class BaseEntity {

    public static final String ID_GEN = "sequence_generator";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = ID_GEN)
    private Long id;

    @CreatedDate
    private Timestamp created;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    @PrePersist
    protected void onCreate() {
	created = new Timestamp(System.currentTimeMillis());
    }

    @Override
    public boolean equals(Object o) {
	if (this == o) {
	    return true;
	}
	if (o == null || getClass() != o.getClass()) {
	    return false;
	}
	BaseEntity entity = (BaseEntity) o;
	if (entity.id == null || this.id == null) {
	    return false;
	}
	return Objects.equals(entity.id, this.id);
    }

    @Override
    public int hashCode() {
	return Objects.hashCode(id);
    }

}
