package com.igor.merchants.dal.entity.transactions;

public enum TransactionStatus {
	APPROVED, REVERSED, REFUNDED, ERROR;
}