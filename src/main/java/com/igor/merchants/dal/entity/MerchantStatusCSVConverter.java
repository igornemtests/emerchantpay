package com.igor.merchants.dal.entity;


import com.opencsv.bean.AbstractBeanField;
import com.opencsv.exceptions.CsvConstraintViolationException;
import com.opencsv.exceptions.CsvDataTypeMismatchException;

public class MerchantStatusCSVConverter extends AbstractBeanField<MerchantStatus, String> {  
	      @Override  
	      protected MerchantStatus convert(String value) throws CsvDataTypeMismatchException, CsvConstraintViolationException {  
	           try {  
	                return MerchantStatus.valueOf(value);  
	           } catch (RuntimeException e) {  
	                throw new CsvDataTypeMismatchException(e.getMessage());  
	           }  
	      }  
	   
}
