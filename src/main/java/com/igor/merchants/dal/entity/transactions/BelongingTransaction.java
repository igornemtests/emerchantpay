package com.igor.merchants.dal.entity.transactions;

import java.math.BigDecimal;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class BelongingTransaction<T extends Transaction> extends Transaction {

    @JsonIgnore
    @OneToOne(targetEntity = Transaction.class, cascade = { CascadeType.PERSIST })
    private T origin;

    @Transient
    private UUID originUuid;

    public BelongingTransaction() {
    }

    public BelongingTransaction(UUID uuid, UUID originUuid, BigDecimal amount, String customerEmail,
	    String customerPhone) {
	super(uuid, amount, customerEmail, customerPhone);
	this.originUuid = originUuid;
    }

    public T getOrigin() {
	return origin;
    }

    public void setOrigin(T origin) {
	this.origin = origin;
    }

    public UUID getOriginUuid() {
	return originUuid;
    }

    public void setOriginUuid(UUID originUuid) {
	this.originUuid = originUuid;
    }

}
