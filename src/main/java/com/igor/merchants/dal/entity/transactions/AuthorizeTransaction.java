package com.igor.merchants.dal.entity.transactions;

import java.math.BigDecimal;
import java.util.UUID;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@DiscriminatorValue(Transaction.Discriminator.AUTHORIZE)
public class AuthorizeTransaction extends Transaction {

    public AuthorizeTransaction() {
    }

    @JsonCreator
    public AuthorizeTransaction(@JsonProperty(required = true) UUID uuid,
	    @JsonProperty(required = true) BigDecimal amount, @JsonProperty(required = true) String customerEmail,
	    String customerPhone) {
	super(uuid, amount, customerEmail, customerPhone);
    }
}
