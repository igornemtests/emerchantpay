package com.igor.merchants;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.igor.merchants.service.MerchantsLoader;
import com.igor.merchants.service.TransactionSevice;
import com.igor.merchants.service.UsersLoader;

/**
 * Main application.
 *
 */
@SpringBootApplication
@EnableAutoConfiguration
@EnableScheduling
public class MerchantsApplication extends WebMvcConfigurerAdapter {

    @Value("${transaction.expiration}")
    private int expired;

    @Autowired
    private TransactionSevice transactions;

    /**
     *
     * @param args - in first parameter should be supplied CSV file path containing
     *             merchant's list to be loaded
     */
    public static void main(String... args) {
	if (args.length < 2) {
	    System.out.println(
		    "Please supply in first argument CSV file path with merchants list, and in second argument CSV file path to user credentials to be loaded");
	} else {
	    SpringApplication.run(MerchantsApplication.class, args);
	}
    }

    /**
     * Load merchants right after application start.
     * 
     */
    @Bean
    public CommandLineRunner demo(MerchantsLoader merchantsLoader, UsersLoader usersLoader) {
	return (args) -> {
	    merchantsLoader.load(args[0]);
	    usersLoader.load(args[1]);
	};
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
	super.configureMessageConverters(converters);
	converters.add(new MappingJackson2HttpMessageConverter());
    }

    @Scheduled(fixedDelay = 30000)
    public void scheduleFixedDelayTask() {
	transactions.deleteExpired();
    }
}
