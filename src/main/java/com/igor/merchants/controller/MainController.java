package com.igor.merchants.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.igor.merchants.dal.entity.Merchant;
import com.igor.merchants.dal.entity.MerchantStatus;
import com.igor.merchants.dal.entity.Role;
import com.igor.merchants.dal.entity.User;
import com.igor.merchants.dal.entity.transactions.Transaction;
import com.igor.merchants.service.MerchantSevice;
import com.igor.merchants.service.TransactionSevice;

/**
 * Main Controller. Serves GUI requests
 */
@Controller
@SessionAttributes("errors")
public class MainController {
    private static final String ROLE_ADMIN = "hasAuthority('ADMIN')";
    private static final String ROLE_USER = "hasAuthority('USER')";
    private static final ModelAndView MAIN_MODEL_AND_VIEW = new ModelAndView("redirect:/");

    @Autowired
    private MerchantSevice merchantService;

    @Autowired
    private TransactionSevice transactionService;

    @ModelAttribute("errors")
    public List<String> errors() {
	return new ArrayList<String>();
    }

    @GetMapping("/")
    public String homePage(UsernamePasswordAuthenticationToken token, Model model,
	    @ModelAttribute("errors") List<String> errors) {
	removeExpired();
	User user = ((User) token.getPrincipal());
	if (user.getRole() == Role.ADMIN) {
	    model.addAttribute("merchant", new Merchant());
	    Iterable<Merchant> merchants = merchantService.getMerchants();
	    model.addAttribute("merchants", merchants);
	} else {
	    model.addAttribute("merchant", merchantService.getMerchant(user.getMerchantId()));
	}

	Iterable<Transaction> transactions = transactionService.getTransactions(user);
	model.addAttribute("transactions", transactions);

	if (errors != null && errors.size() > 0) {
	    String error = errors.toString();
	    model.addAttribute("error", error.substring(1, error.length() - 1));
	    errors.clear();
	}
	return "home";
    }

    public void removeExpired() {
	transactionService.deleteExpired();
    }

    @PreAuthorize(ROLE_ADMIN)
    @RequestMapping(value = "/merchant/create", method = RequestMethod.POST)
    public ModelAndView createMerchant(@Valid @ModelAttribute(value = "merchant") Merchant merchant, BindingResult res,
	    RedirectAttributes attributes) {
	removeExpired();
	if (res.hasErrors()) {
	    return resolveValidationErrors(res, attributes);
	}
	if (merchant.getTotalTransactionSum() == null) {
	    merchant.setTotalTransactionSum(BigDecimal.ZERO);
	}
	if (merchant.getStatus() == null) {
	    merchant.setStatus(MerchantStatus.ACTIVE);
	}
	merchantService.addMerchant(merchant);
	return MAIN_MODEL_AND_VIEW;
    }

    @PreAuthorize(ROLE_ADMIN)
    @RequestMapping(value = "/merchant/update", method = RequestMethod.POST)
    public ModelAndView updateMerchant(@Valid @ModelAttribute(value = "merchant") Merchant merchant, BindingResult res,
	    RedirectAttributes attributes) {
	removeExpired();
	if (res.hasErrors()) {
	    return resolveValidationErrors(res, attributes);
	}
	merchantService.updateMerchant(merchant);
	return MAIN_MODEL_AND_VIEW;
    }

    @PreAuthorize(ROLE_ADMIN)
    @RequestMapping(value = "/merchant/delete", method = RequestMethod.POST)
    public ModelAndView deleteMerchant(@ModelAttribute(value = "merchant") Merchant merchant,
	    RedirectAttributes attributes) {
	removeExpired();
	try {
	    merchantService.removeMerchant(merchant.getId());
	} catch (Exception e) {
	    List<String> errors = new ArrayList<String>();
	    errors.add(e.getMessage());
	    attributes.addFlashAttribute("errors", errors);
	}
	return MAIN_MODEL_AND_VIEW;
    }

    @PostMapping("/transaction/create")
    @PreAuthorize(ROLE_USER)
    public ModelAndView addTransaction(UsernamePasswordAuthenticationToken token, @RequestParam String trn,
	    RedirectAttributes attributes) throws JsonMappingException, JsonProcessingException {
	removeExpired();
	ObjectMapper mapper = new ObjectMapper();
	mapper.registerModule(new ParameterNamesModule());
	Transaction transaction = null;

	ModelAndView model = MAIN_MODEL_AND_VIEW;
	try {
	    transaction = mapper.readValue(trn, Transaction.class);
	    transactionService.createTransaction(((User) token.getPrincipal()).getMerchantId(), transaction);
	} catch (Exception e) {
	    List<String> errors = new ArrayList<String>();
	    errors.add(e.getMessage());
	    attributes.addFlashAttribute("errors", errors);
	}
	return model;
    }

    private ModelAndView resolveValidationErrors(BindingResult res, RedirectAttributes attributes) {
	List<String> errors = new ArrayList<String>();
	errors.addAll(res.getAllErrors().stream().map(ObjectError::getDefaultMessage).collect(Collectors.toList()));
	attributes.addFlashAttribute("errors", errors);
	return MAIN_MODEL_AND_VIEW;
    }
}