package com.igor.merchants.exception;

public class TransactionCreationException extends Exception {

    public TransactionCreationException(String message, Throwable cause) {
	super(message, cause);
    }

    public TransactionCreationException(String message) {
	super(message);
    }

    public TransactionCreationException(Throwable cause) {
	super(cause);
    }

}
