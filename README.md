# Setup

- Uncomment Postgres database setup in <project-root>/src/main/resources/application.properties and comment current setup of H2.

- Currently transaction expiration time is set to 60 minutes accordingly to task requirements, but you can configure it by modifying "transaction.expiration" in application.properties

- Job which removes expired transactions will be run every 30 second(it defined hard coded in MerchantsApplication.scheduleFixedDelayTask()), but to allow the deletion be up to time every GUI request also performs this check and remove expired transactions

- Checkstyle is configured at <project root>/checkstyle.xml

- Upon application start will be imported 4 merchants, to modify them - <project-root>/assembly/merchants.csv

- Upon application start will be imported and available for login 3 users(to modify them - <project-root>/assembly/users.csv): 
one with role ADMIN (admin:admin) and 2 with role USER : m1:m1(merchant1's user), m2:m2(merchant2's user)

# Build

mvn clean install

# Testing

- Unit and Integration test are located at <project root>/src/test/java at package com.igor.merchants 
- mvn clean install will run them too

# Build
mvn clean install

# Testing

Unit and Integration tests are located at <project root>/src/test/java at package com.igor.merchants .
Maven will run them as usual  

# Usage

- Main class: com.igor.merchants.MerchantApplication to run.

- Run the application with two parameters: path to CSV file with merchants, and path to CSV file with users,
currently they are at: <project-root>/assembly/[merchants.csv|users.csv]

- Open main page at [localhost:8080](http://localhost:8080)

- When you logged in as Admin you allowed create/modify/delete merchants, but whe you logged in as User then you allowed to add transactions and they will be assigned to user's merchant

* Jsons examples for transactions located in file <project root>/assembly/transactions-examples.json, legend for transaction types:
  ##### AUTHORIZE = "A"
  ##### CHARGE = "C"
  ##### REFUND = "B"
  ##### REVERSAL = "R"
  ##### ERROR = "E"

- Exceptional situations will be shown at GUI top written by red bold font, not all errors translated to human readable format due to task  large volume, therefore you will meet sometime error messages with debug information.
